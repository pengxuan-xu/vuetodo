## Development
We use [Yarn](https://yarnpkg.com/en/) in this project. Replace `yarn` with `npm` if you prefer to use npm.
node version > 8.0.0 is recommended to run this project.

To get up and running run:
``` bash
$ yarn
$ yarn run api-server
$ yarn run dev
```

## Production
To build for production:
``` bash
$ npm run build
```
- JavaScript minified with [UglifyJS](https://github.com/mishoo/UglifyJS2).
- JavaScript code spliting with [Webpack 3 Dynamic Imports](https://webpack.js.org/guides/code-splitting/#dynamic-imports).
- HTML minified with [html-minifier](https://github.com/kangax/html-minifier).
- CSS across all components extracted into a single file and minified with [cssnano](https://github.com/ben-eb/cssnano).
- All static assets compiled with version hashes for efficient long-term caching.

## Unit Test
To run unit test:
``` bash
$ npm test
```
 - Unit Tests are provided by Karma, Mocha, Chai and Sinon-Chai.
 - Unit Tests are configured to run with PhantomJS.
 - The testing files should be place under **test/unit/specs** folder.
 - Make sure every test file ends in `.spec.js`.
 - Test converage reports can be found in **tests/unit**.

## Directory Structure
##### main.js
This file will load your single page application and bootstrap all the plugins that are used.
It will also serve as the entry point which will be loaded and compiled using webpack.

##### src/App.vue
The main Vue file.
This file will load the page inside the `router-view`-component.

##### app.global.css
The global css stylesheet.
Always defined **scoped** styles in vue components for easier debug.

##### assets/
Fonts, images, etc.

##### components/
Vue components.

##### layouts/
Vue components that function as page layouts.

##### mixins/
Vue mixins.

##### pages/
Vue components that function as pages. You should import the pages in **routes/**.

##### plugins/
Vue plugins. We use `Vue.use` to include Plugins in `Vue.prototype`.
We are using:
 - axios
 - bootstrap-vue
 - vuex
 - vue-router
 - vuex-router-sync

##### proxies/
Proxies are used to perform AJAX-requests. They are specially created for RESTful apis.
Create a new proxy, for example `UserProxy`, and extend the `Proxy`.
This way you've got access to the `all`, `find`, `update`, `create` and `destroy` methods.

##### routes/
VueRouter routes.

##### store/
Vuex store. We configure one type of state as a module. Modules are namespaced and each module should have:
 - actions
 - getters
 - mutations
 - mutation-types
 - state

##### utils/
utils functions.

## Mock api server
This project comes with a mock api server implemented with [Express](https://expressjs.com/) and [node-sqlite3](https://github.com/mapbox/node-sqlite3). The server scripts is located at **server/mock-api-server.js**.
We use `todo.db` as the database. `todo.db.schema` is an empty db contains only the schema. You may copy `todo.db.schema` to `todo.db` to reset the database.

