import TaskProxy from '@/proxies/TaskProxy';
import * as types from './mutation-types';

export const all = ({ commit }) => {
  new TaskProxy().all().then((res) => {
    res.reverse().forEach((task) => {
      commit(types.ADD_TASK, task);
    });
  });
};

export const add = ({ commit }, task) => {
  new TaskProxy().create(task).then((createdTask) => {
    commit(types.ADD_TASK, createdTask);
  });
};

export const remove = ({ commit }, taskId) => {
  new TaskProxy().destroy(taskId).then(() => {
    commit(types.REMOVE_TASK, taskId);
  });
};

export const done = ({ commit, state }, taskId) => {
  const task = state.tasks[taskId];
  const nextTask = { ...task, is_completed: true };
  new TaskProxy().update(taskId, nextTask).then(() => {
    commit(types.MARK_TASK_AS_COMPLETED, task);
  });
};

export const undone = ({ commit, state }, taskId) => {
  const task = state.tasks[taskId];
  const nextTask = { ...task, is_completed: false };
  new TaskProxy().update(taskId, nextTask).then(() => {
    commit(types.MARK_TASK_AS_INCOMPLETED, task);
  });
};

export default {
  all,
  add,
  remove,
  done,
  undone,
};
