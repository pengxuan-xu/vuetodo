export default [
  // About
  {
    path: '/about',
    name: 'about.index',
    component: () => import('@/pages/About/Index'),
  },
  // Task
  {
    path: '/tasks',
    name: 'task.index',
    component: () => import('@/pages/Task/Index'),
  },

  // redirections
  {
    path: '/',
    redirect: '/tasks',
  },
  {
    path: '/*',
    redirect: '/tasks',
  },
];
