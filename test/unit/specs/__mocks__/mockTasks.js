import _cloneDeep from 'lodash/cloneDeep';
import * as types from '@/store/modules/tasks/mutation-types';

export default function (tasksOriginal, initialState, mockTaskList) {
  const tasks = _cloneDeep(tasksOriginal);
  
  tasks.state = initialState || tasks.state;

  sinon.stub(tasks.actions, 'all').callsFake(({ commit }) => {
    mockTaskList.forEach((mockTask) => {
      commit(types.ADD_TASK, mockTask);
    });
  });

  sinon.stub(tasks.actions, 'add').callsFake(({ commit }, mockTask) => {
    commit(types.ADD_TASK, mockTask);
  });

  sinon.stub(tasks.actions, 'remove').callsFake(({ commit }, mockTaskId) => {
    commit(types.REMOVE_TASK, mockTaskId);
  });
  
  sinon.stub(tasks.actions, 'done').callsFake(({ commit }, mockTaskId) => {
    commit(types.MARK_TASK_AS_COMPLETED, { mockTaskId });
  });
  
  sinon.stub(tasks.actions, 'undone').callsFake(({ commit }, mockTaskId) => {
    commit(types.MARK_TASK_AS_INCOMPLETED, { mockTaskId });
  });

  return tasks;
}
