import Vue from 'vue';
import Vuex from 'vuex';
import TaskCreator from '@/components/TaskCreator';
import tasks from '@/store/modules/tasks';

import mockTasks from './__mocks__/mockTasks';
import { vmNextTick } from './__utils__';

const mockTask = {
  id: 1,
  title: 'a task',
  created_at: '2017-12-16 14:30:00',
  is_completed: 0,
};

const mockedTasks = mockTasks(tasks, null, []);

const storeStub = new Vuex.Store({ modules: { tasks: mockedTasks }, strict: false });

describe('TaskCreator', () => {
  let vm;

  beforeEach(() => {
    const Comp = Vue.extend({ ...TaskCreator, store: storeStub });
    vm = new Comp({ propsData: { taskId: mockTask.id } }).$mount();
  });

  it('should sets empty newTask data', () => {
    expect(vm.newTask).to.deep.equal({ title: '' });
  });

  it('should render a form', () => {
    expect(vm.$el.tagName.toLowerCase()).to.equal('form');
  });

  it('should render an input', () => {
    expect(vm.$el.querySelector('input')).to.exist;
  });

  it('input change should be binded to newTask.title', () => {
    const el = vm.$el.querySelector('input');
    el.value = mockTask.title;
    el.dispatchEvent(new Event('change'));
    vmNextTick(vm).then(() => {
      expect(vm.newTask.title).to.equal(mockTask.title);
    });
  });

  it('should render a submit button', () => {
    const el = vm.$el.querySelector('button');
    expect(el.type).to.equal('submit');
  });

  it('submitting form should add new task', () => {
    expect(vm.$store.getters['tasks/getValidTasks']).to.be.empty;
    vm.newTask = mockTask;
    vm.$el.querySelector('button').dispatchEvent(new Event('click'));
    expect(vm.$store.getters['tasks/getValidTasks']).to.have.lengthOf(1);
  });

  it('submitting form should empty input', () => {
    vm.newTask = mockTask;
    vm.$el.querySelector('button').dispatchEvent(new Event('click'));
    expect(vm.newTask.title).to.be.empty;
  });
});
