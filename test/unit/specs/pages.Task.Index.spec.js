import Vuex from 'vuex';
import { shallow, createLocalVue } from 'vue-test-utils';
import PagesTaskIndex from '@/pages/Task/Index';
import TaskCreator from '@/components/TaskCreator';
import TaskItem from '@/components/TaskItem';
import TaskEmpty from '@/components/TaskEmpty';

import Layout from './__mocks__/Layout';

const localVue = createLocalVue();
localVue.use(Vuex);

const storeStubEmpty = new Vuex.Store({
  actions: {
    'tasks/all': sinon.spy(),
  },
  getters: {
    'tasks/taskList': () => [],
    'tasks/hasNoTask': () => true,
  },
});

const storeStubNonEmpty = new Vuex.Store({
  actions: {
    'tasks/all': sinon.spy(),
  },
  getters: {
    'tasks/taskList': () => [1],
    'tasks/hasNoTask': () => false,
  },
});

describe('PagesTaskIndex', () => {
  let wrapper;

  it('should render task creator', () => {
    wrapper = shallow(PagesTaskIndex, { store: storeStubEmpty, localVue, stubs: { 'core-layout': Layout } });
    expect(wrapper.contains(TaskCreator)).to.be.true;
  });

  describe('when task is empty', () => {
    beforeEach(() => {
      wrapper = shallow(PagesTaskIndex, { store: storeStubEmpty, localVue, stubs: { 'core-layout': Layout } });
    });

    it('should render TaskEmpty', () => {
      expect(wrapper.contains(TaskEmpty)).to.be.true;
    });
  });

  describe('when task is not empty', () => {
    beforeEach(() => {
      wrapper = shallow(PagesTaskIndex, { store: storeStubNonEmpty, localVue, stubs: { 'core-layout': Layout } });
    });

    it('should render TaskItem', () => {
      expect(wrapper.contains(TaskItem)).to.be.true;
    });
  });
});
