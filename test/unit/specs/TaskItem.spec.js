import Vue from 'vue';
import Vuex from 'vuex';
import TaskItem from '@/components/TaskItem';
import tasks from '@/store/modules/tasks';

import mockTasks from './__mocks__/mockTasks';
import { vmNextTick } from './__utils__';

const mockTask = {
  id: 1,
  title: 'a task',
  created_at: '2017-12-16 14:30:00',
  is_completed: 0,
};

const initialState = {
  tasks: { 1: mockTask },
  taskList: [1],
};

const mockedTasks = mockTasks(tasks, initialState);

const storeStub = new Vuex.Store({ modules: { tasks: mockedTasks }, strict: false });

describe('TaskItem', () => {
  let vm;

  beforeEach(() => {
    const Comp = Vue.extend({ ...TaskItem, store: storeStub });
    vm = new Comp({ propsData: { taskId: mockTask.id } }).$mount();
  });

  it('should render a checkbox', () => {
    expect(vm.$el.querySelector('.checkbox')).to.exist;
  });

  it('checkbox input should be unchecked', () => {
    expect(vm.$el.querySelector('input[type="checkbox"]').checked).to.be.false;
  });

  it('clicking checkbox should toggle task between done and undone', () => {
    const el = vm.$el.querySelector('input[type="checkbox"]');
    el.dispatchEvent(new Event('click'));
    expect(vm.taskDone).to.be.ture;
    el.dispatchEvent(new Event('click'));
    expect(vm.taskDone).to.be.false;
  });

  it('checkbox input should be reflect task completion status', () => {
    const el = vm.$el.querySelector('input[type="checkbox"]');
    expect(el.checked).to.be.false;
    el.dispatchEvent(new Event('click'));
    vmNextTick(vm).then(() => {
      expect(el.checked).to.be.true;
    });
  });

  it('should render task title', () => {
    vmNextTick(vm).then(() => {
      expect(vm.$el.querySelector('input[type="checkbox"]').textContent).to.include(vm.taskTitle);
    });
  });

  it('should show close icon on hover', () => {
    const wrapper = vm.$el;
    const el = vm.$el.querySelector('svg');
    expect(el).not.to.exist;

    wrapper.dispatchEvent(new Event('mouseover'));
    vmNextTick(vm).then(() => {
      expect(el).to.exist;
      wrapper.dispatchEvent(new Event('mouseleave'));
      return vmNextTick(vm);
    }).then(() => {
      expect(el).not.to.exist;
    });
  });

  it('clicking close icon should remove the task', () => {
    const spy = sinon.spy();
    sinon.stub(vm.$store, 'dispatch').callsFake(spy);
    vm.$el.dispatchEvent(new Event('mouseover'));
    vmNextTick(vm).then(() => {
      vm.$el.querySelector('svg').dispatchEvent(new Event('click'));
      expect(spy.calledWith('tasks/remove')).to.be.true;
    });
  });
});
